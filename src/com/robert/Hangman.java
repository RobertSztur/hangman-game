package com.robert;

public class Hangman {

    //ilość dopuszczalnych błędnych trafień
    private static final int MAX_MISTAKES = 8;

    //słowo do zgadnięcia
    private String guessWord;
    //wylosowane słowo do zgadnięcia
    private String guessWordDisplay;
    //tablica znaków które wytypował gracz
    private char[] userGuesses;
    //ilość prób
    private int guesses;
    //ilość błędnych prób
    private int mistakes;

    public Hangman(String guessWord) {
        this.guessWord = guessWord;
        userGuesses = new char[32];
        generateDisplay();
    }

    public String getGuessWord() {
        return guessWord;
    }

    public String getGuessWordDisplay() {
        return guessWordDisplay;
    }

    public boolean userLost() {
        return mistakes >= MAX_MISTAKES;
    }

    public boolean userWon() {
        return !guessWordDisplay.contains("*");
    }



    public void checkLetter(char letter) {
        if(!arrayContains(userGuesses, letter)) {
            checkMistake(letter);
            rememberGuess(letter);
            generateDisplay();
        }
    }

    private void checkMistake(char letter) {
        if(guessWord.indexOf(letter) == -1) {
            mistakes++;
            System.out.println("pudło");
        }else
        System.out.println("trafiłeś");
    }

    private void rememberGuess(char letter) {
        userGuesses[guesses] = letter;
        guesses++;
    }

    private void generateDisplay() {
        String display = "";
        for (int i = 0; i < guessWord.length(); i++) {
            char nextChar = guessWord.charAt(i);
            if(arrayContains(userGuesses, nextChar))
                display += nextChar;
            else if(nextChar == ' ')
                display += ' ';
            else
                display += "*";
        }
        this.guessWordDisplay = display;
    }

    private boolean arrayContains(char[] array, char letter) {
        for (char element : array) {
            if (element == letter)
                return true;
        }
        return false;
    }


}
